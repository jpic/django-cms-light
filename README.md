# Install

## In LXD

Use the ansible playbooks to setup an Ubuntu Xenial container:

    cd ansible/
    ansible-galaxy install -r requirements.yaml -p roles
    ansible-playbook --sudo lxd.yml

Then, use the lxc command to get the container ip:

    lxc exec dclxenial ip a

Enter the container with:

    lxc exec dclxenial bash

In the container, sync demo sites with:

    su dcl -c 'MEDIA_ROOT=/srv/dcl/media /srv/dcl/env/bin/dcl dcl_sync https://gitlab.com/jpic/jpic-sites.git'

In the container, change localhost to the container ip:

    su dcl -c '/srv/dcl/env/bin/dcl shell'
    Python 3.5.1+ (default, Mar 30 2016, 22:46:26)
    [GCC 5.3.1 20160330] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    (InteractiveConsole)
    >>> from django.contrib.sites.models import Site
    >>> Site.objects.filter(domain='localhost').update(domain='10.0.40.13')
    1

Note: for some reason I had to stop the dcl service, remove /tmp/dcl.sock,
and start it again. But then I could open dcl in the browser and see the
website.

## On an Ubuntu Xenial server

Clone dcl, in ansible/ run:

    ansible-playbook -i yourip, --user root site.yml  -v

Then, ssh and change to dcl user and dcl user home directory, and configure
the example site:

    MEDIA_ROOT=/srv/dcl/media /srv/dcl/env/bin/dcl dcl_sync https://gitlab.com/jpic/jpic-sites.git

And update the default domain from localhost to whatever you want to use (ie.
your ip):

    /srv/dcl/env/bin/dcl shell
    Python 3.5.1+ (default, Mar 30 2016, 22:46:26)
    [GCC 5.3.1 20160330] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    (InteractiveConsole)
    >>> from django.contrib.sites.models import Site
    >>> Site.objects.filter(domain='localhost').update(domain='10.0.40.13')
    1

## Manual

Create a python3 virtualenv and install dcl:

    $ virtualenv3 dcl_env
    $ source dcl_env/bin/activate
    $ pip install -e git+https://gitlab.com/yourlabs/dcl.git#egg=dcl
    $ pip install -r dcl_env/src/dcl/requirements.txt

Create a postgresql database:

    [dcl_env] 12/07 2016 11:56:05 jpic@lue ~/dcl
    $ createdb -O jpic -E UTF8 dcl

Then, apply schema migrations:

    [dcl_env] 12/07 2016 11:56:16 jpic@lue ~/dcl
    $ dcl migrate
    Operations to perform:
      Apply all migrations: admin, default, sites, account, contenttypes, eventlog, auth, easy_thumbnails, dcl_sites, sessions
    Running migrations:
      Rendering model states... DONE
      Applying contenttypes.0001_initial... OK
      Applying auth.0001_initial... OK
      Applying account.0001_initial... OK
      Applying account.0002_fix_str... OK
      Applying account.0003_auto_20160711_1113... OK
      Applying admin.0001_initial... OK
      Applying admin.0002_logentry_remove_auto_add... OK
      Applying contenttypes.0002_remove_content_type_name... OK
      Applying auth.0002_alter_permission_name_max_length... OK
      Applying auth.0003_alter_user_email_max_length... OK
      Applying auth.0004_alter_user_username_opts... OK
      Applying auth.0005_alter_user_last_login_null... OK
      Applying auth.0006_require_contenttypes_0002... OK
      Applying auth.0007_alter_validators_add_error_messages... OK
      Applying default.0001_initial... OK
      Applying default.0002_add_related_name... OK
      Applying default.0003_alter_email_max_length... OK
      Applying easy_thumbnails.0001_initial... OK
      Applying easy_thumbnails.0002_thumbnaildimensions... OK
      Applying eventlog.0001_initial... OK
      Applying eventlog.0002_auto_20150113_1450... OK
      Applying eventlog.0003_auto_20160111_0208... OK
      Applying sites.0001_initial... OK
      Applying sites.0002_alter_domain_unique... OK
      Applying dcl_sites.0001_initial... OK
      Applying dcl_sites.0002_sitepage_background... OK
      Applying dcl_sites.0003_auto_20160711_2202... OK
      Applying dcl_sites.0004_change_title_to_menu_title... OK
      Applying dcl_sites.0005_add_title_for_404... OK
      Applying dcl_sites.0006_metadata... OK
      Applying sessions.0001_initial... OK

# Setup

Activate the virtualenv:

	$ source env/bin/activate

Add the example sites:

    $ MEDIA_ROOT=media dcl dcl_sync https://gitlab.com/jpic/jpic-sites

To update your websites, run:

    $ MEDIA_ROOT=media dcl dcl_sync

If you do not intend to open the website on localhost:

    $ dcl shell
    from django.contrib.sites.models import Site
    Site.objects.filter(domain=localhost).update(domain='yourdomainorip')

To run the development server on `http://localhost:8000`:

    $ MEDIA_ROOT=media dcl runserver

To access the admin at `/admin/`, create your administrator user:

    [dcl_env] 12/07 2016 11:56:19 jpic@lue ~/dcl
    $ dcl createsuperuser
    Username (leave blank to use 'jpic'):
    Email address: t@tt.tt
    Password:
    Password (again):
    Superuser created successfully.

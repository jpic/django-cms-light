/* global window */
window.jQuery = window.$ = require('jquery');

const $ = window.$;

require('bootstrap');

$(() => {
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });

    window.onpopstate = function (e) {
        window.loadPage(e.state.html);
    };

    window.loadPage = function (data) {
        const doc = $(`<doc>${data}</doc>`);
        function copyAttrs(selector) {
            const attrs = doc.find(selector).get(0).attributes;
            for (let i = 0; i < attrs.length; i++) {
                $(selector).attr(attrs[i].name, attrs[i].value);
            }
        }

        $('body').attr('class', doc.find('body').attr('class'));
        copyAttrs('#body-container');
        $('#content-body').html($(data).find('#content-body').html());
        return doc;
    };

    // This needs more local testing before hitting production
    // Require manual feature flip, for badass hacker only
    /*
    $('a[href]').on('click', function (e) {
        const url = $(this).attr('href');

        if (!url.startsWith('/')) {
            return e;
        }

        e.preventDefault();
        e.stopPropagation();

        $.ajax(url, {
            contentType: 'application/json; charset=UTF-8',
            success(data) {
                const doc = window.loadPage(data);
                window.history.pushState({html: data}, doc.find('title').html(), url);
            }
        });
    });
    */
});

require('picturefill');
require('lazysizes/plugins/progressive/ls.progressive');
require('lazysizes/plugins/unveilhooks/ls.unveilhooks');
require('lazysizes/plugins/optimumx/ls.optimumx');
require('lazysizes/plugins/parent-fit/ls.parent-fit');
require('lazysizes/plugins/bgset/ls.bgset');
require('lazysizes');

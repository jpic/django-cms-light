import markdown
import subprocess
import os

from dateutil.parser import parse

from django.conf import settings
from django.contrib.sites.models import Site
from django.template.defaultfilters import striptags

from easy_thumbnails.files import get_thumbnailer

from dcl_sites.cache import (
    DomainCache,
    DomainLangageCache,
    DomainUrlCache
)
from dcl_sites.models import (
    Alias,
    ContentFile,
    RepositoryRef,
    SiteRef,
    Directory
)
from dcl_sites.utils import process_image

from PIL import Image

import yaml


def sync_repository(repository):
    repo_path = os.path.join(
        settings.MEDIA_ROOT,
        'repositories',
        str(repository.pk),
    )

    if not os.path.exists(repo_path):
        try:
            subprocess.check_call([
                'git',
                'clone',
                repository.url,
                repo_path,
            ])
        except subprocess.CalledProcessError:
            return

    cwd = os.getcwd()
    os.chdir(repo_path)
    subprocess.check_call(['git', 'fetch'])
    out = subprocess.check_output(['git', 'branch', '-lr']).decode()
    for line in out.split('\n'):
        branch = line.strip().split(' ')[0]
        if branch in ('origin/HEAD', ''):
            continue
        subprocess.check_call([
            'git',
            'reset',
            '--hard',
            branch,
        ])
        ref, c = RepositoryRef.objects.get_or_create(
            repository=repository,
            name=branch.replace('origin/', ''),
        )

        site_metadata_path = os.path.join(
            repo_path,
            'dcl.yaml',
        )
        if os.path.exists(site_metadata_path):
            with open(site_metadata_path, 'r') as f:
                ref.metadata = yaml.load(f.read())
            ref.save()

        for site in ref.metadata.get('sites', []):
            name = site.get('name', site['domain'])

            try:
                site = Site.objects.get(domain=site['domain'])
            except Site.DoesNotExist:
                site = Site.objects.create(
                    domain=site['domain'],
                    name=name
                )

            if site.name != name:
                site.name = name
                site.save()

            # Expect conflicts, first branch wins, admin can handle it in the
            # administration interface
            if not SiteRef.objects.filter(site=site).count():
                SiteRef.objects.create(
                    site=site,
                    ref=ref,
                )

        sites = ref.metadata.get('sites', [])
        if not sites:
            continue

        main_site_domain = sites[0]['domain']
        main_site = Site.objects.get(domain=main_site_domain)
        for alias in ref.metadata.get('aliases', []):
            site_name = '%s -> %s' % (alias, main_site_domain)

            try:
                site = Site.objects.get(domain=alias)
            except Site.DoesNotExist:
                site = Site.objects.create(domain=alias, name=site_name)

            if site_name != site.name:
                site.name = site_name
                site.save()

            Alias.objects.get_or_create(source=main_site, alias=site)
    os.chdir(cwd)


def sync_ref(ref):
    images_path = os.path.join(
        settings.MEDIA_ROOT,
        'images',
    )
    if not os.path.exists(images_path):
        os.makedirs(images_path)

    repo_path = ref.repo_path

    if not os.path.exists(os.path.dirname(repo_path)):
        os.makedirs(os.path.dirname(repo_path))

    if not os.path.exists(repo_path):
        try:
            subprocess.check_call([
                'git',
                'clone',
                ref.repository.url,
                repo_path,
            ])
        except subprocess.CalledProcessError:
            return

    cwd = os.getcwd()
    os.chdir(repo_path)
    subprocess.check_call(['git', 'stash'])
    subprocess.check_call(['git', 'fetch'])
    try:
        subprocess.check_call([
            'git',
            'reset',
            '--hard',
            'origin/%s' % ref.name,
        ])
    except subprocess.CalledProcessError:
        print('Skip %s' % ref.name)
        return
    os.chdir(cwd)

    website_path = ref.website_path

    def read_file(*path):
        p = os.path.join(*path)
        if not os.path.exists(p):
            return ''
        with open(p, 'r') as f:
            return f.read()

    ref.script = read_file(website_path, 'site.js')
    ref.style = read_file(website_path, 'site.css')
    ref.extra_head = read_file(website_path, 'head.html')
    ref.extra_body = read_file(website_path, 'body.html')
    ref.save()

    first = None
    for root, dirs, files in os.walk(website_path):
        if '/.git' in root:
            continue

        rel_path = os.path.relpath(root, website_path).strip('/')
        if rel_path == '.':
            continue

        parent = None
        if '/' in rel_path:
            parent = Directory.objects.get(
                rel_path='/'.join(rel_path.split('/')[:-1]),
                ref=ref,
            )

        directory, directory_created = Directory.objects.get_or_create(
            rel_path=rel_path,
            ref=ref,
            parent=parent,
        )

        if first is None:
            # That'll be our index if dcl.yaml doesn't define site
            first = directory

        page_metadata_path = os.path.join(
            root,
            'page.yaml',
        )
        if os.path.exists(page_metadata_path):
            with open(page_metadata_path, 'r') as f:
                directory.metadata = yaml.load(f.read())
        else:
            directory.metadata = {}

        if 'publish_datetime' in directory.metadata:
            directory.publish_datetime = parse(
                directory.metadata['publish_datetime'],
            )

        background = os.path.abspath(os.path.join(root, 'background.jpg'))
        if os.path.exists(background):
            out = subprocess.check_output(['md5sum', background])
            md5 = out.split(b' ')[0].decode()
            destination = os.path.join(
                images_path,
                '%s.jpg' % md5
            )
            image = Image.open(background)
            image = process_image(
                image,
                directory.metadata.get('background', {})
            )
            image.save(destination)
            directory.background = os.path.relpath(
                destination,
                settings.MEDIA_ROOT
            )
        else:
            directory.background = ''
        directory.save()

        for f in files:
            if not f.endswith('.md'):
                continue
            language = f.rstrip('.md')

            content, c = ContentFile.objects.get_or_create(
                directory=directory,
                language=language,
            )

            with open(os.path.join(root, f), 'r') as fh:
                content.source = fh.read()

            # Parse metadata, don't care about the output
            md = markdown.Markdown(extensions=['markdown.extensions.meta'])
            out = md.convert(content.source)
            content.metadata = md.Meta
            # Figure a title
            if not content.metadata.get('title'):
                content.metadata['title'] = [striptags(out.split('\n')[0])]

            # Figure a url
            parent_contentfile = None
            if parent:
                current_parent = parent
                while parent_contentfile is None and current_parent is not None:
                    try:
                        parent_contentfile = current_parent.contentfile_set.get(
                            language=content.language
                        )
                    except ContentFile.DoesNotExist:
                        current_parent = parent.parent

            if parent_contentfile:
                parent_url = parent_contentfile.url
            else:
                parent_url = '/%s/' % content.language

            if 'url' in content.metadata:
                rel_url = ''.join(content.metadata['url'])
            elif parent and parent_contentfile:
                rel_url = os.path.relpath(
                    directory.rel_path,
                    parent_contentfile.directory.rel_path,
                )
            else:
                rel_url = directory.rel_path.split('/')[-1]

            content.url = '%s%s/' % (parent_url, rel_url)

            content.save()

    if len(ref.metadata.get('site', [])):
        first_page = ref.metadata['site'][0]
        if isinstance(first_page, dict):
            first_page_path = first_page['path']
        else:
            first_page_path = first_page

        ref.index = Directory.objects.get(
            ref=ref,
            rel_path=first_page_path,
        )
        ref.save()

        pages = ref.metadata.get('site', [])
        parent = None
        def update_sortkey(pages, parent):
            sortkey = 0
            for page in pages:
                sortkey += 1

                if isinstance(page, dict):
                    path = page['path']
                else:
                    path = page

                if parent:
                    path = '/'.join((parent.rel_path, path))
                directory = Directory.objects.get(
                    ref=ref,
                    rel_path=path,
                )
                directory.sortkey = sortkey
                directory.save()

                if isinstance(page, dict) and 'children' in page:
                    update_sortkey(page['children'], directory)
        update_sortkey(pages, parent)

    ref_directories = Directory.objects.filter(ref=ref)
    for d in ref_directories:
        if not os.path.exists(os.path.join(website_path, d.rel_path)):
            d.delete()

    for f in ContentFile.objects.filter(directory__ref=ref):
        path = os.path.join(
            website_path,
            f.directory.rel_path,
            f.language + '.md'
        )
        if not os.path.exists(path):
            f.delete()


def sync_site(site):
    domain = DomainCache(site.domain)
    domain_data = domain.get_data(refresh=True)

    for language in domain_data['languages']:
        domain_language = DomainLangageCache(site.domain, language)
        domain_language.get_data(refresh=True)
    DomainLangageCache.purge(site.domain, domain_data['languages'])

    urls = ContentFile.objects.filter(
        directory__ref=site.siteref.ref
    ).values_list('url', flat=True)
    for url in urls:
        domain_url = DomainUrlCache(site.domain, url)
        domain_url.get_data(refresh=True)
    DomainUrlCache.purge(site.domain, urls)

from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from easy_thumbnails.files import get_thumbnailer

register = template.Library()


@register.filter
def bgset(background):
    t = get_thumbnailer(background)

    html = ['data-sizes="auto"']
    srcset = []

    for name, config in settings.THUMBNAIL_ALIASES[''].items():
        srcset.append(
            '%s %sw' % (t[name].url, config['size'][0])
        )

    html.append('data-bgset="%s"' % ', '.join(srcset))
    return mark_safe(' '.join(html))

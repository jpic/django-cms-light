from django.conf import settings
from django.core.cache import cache

import jinja2

from .models import (
    Alias,
    ContentFile,
    Directory,
    RepositoryRef,
    SiteRef,
)
from .utils import get_translated_dict


class BaseCache(object):
    def get_data(self, refresh=False):
        key = self.get_key()

        if refresh is False:
            cached = cache.get(key)
            if cached is not None:
                return cached

        data = self.generate_data()
        cache.set(key, data)
        return data

    def render(self, template, **context):
        return jinja2.Template(template).render(**context)


class DomainCache(BaseCache):
    def __init__(self, domain):
        self.domain = domain.split(':')[0]

    def get_key(self):
        return 'dcl_sites:domain:%s' % self.domain

    def generate_data(self):
        ref = RepositoryRef.objects.filter(
            siteref__site__domain=self.domain
        ).first()
        return {
            'alias_of': self.get_alias_of(),
            'languages': self.get_languages(),
            'ref': ref,
            'website_path': ref.website_path,
            'style': self.render(ref.style, ref=ref),
            'script': self.render(ref.script, ref=ref),
            'extra_head': self.render(ref.extra_head, ref=ref),
            'extra_body': self.render(ref.extra_body, ref=ref),
        }

    def get_alias_of(self):
        alias = Alias.objects.filter(alias__domain=self.domain).first()
        if alias:
            return alias.source.domain

    def get_languages(self):
        return set(
            ContentFile.objects.filter(
                directory__ref__siteref__site__domain=self.domain,
            ).values_list(
                'language',
                flat=True
            )
        )

    @staticmethod
    def purge(domains):
        for key in cache.iter_keys('dcl_sites:domain:*'):
            if key not in domains:
                cache.delete(key)


class DomainLangageCache(BaseCache):
    def __init__(self, domain, language):
        self.domain = domain.split(':')[0]
        self.language = language

    def get_key(self):
        return 'dcl_sites:domain_language:%s:%s' % (
            self.domain,
            self.language
        )

    def generate_data(self):
        return {
            'index': self.get_index(),
            'metadata': self.get_metadata(),
            'menu': self.get_menu(),
        }

    def get_index(self):
        return ContentFile.objects.get(
            directory__index_of__siteref__site__domain=self.domain,
            language=self.language,
        ).url

    def get_metadata(self):
        return get_translated_dict(
            self.language,
            SiteRef.objects.filter(
                site__domain=self.domain
            ).first().ref.metadata
        )

    def get_menu(self):
        def add_contents(tree, contents):
            for content in contents:
                menu_item = {
                    'menu_title': content.menu_title,
                    'title': content.title,
                    'url': content.url,
                }

                children = ContentFile.objects.filter(
                    directory__parent=content.directory,
                    language=self.language
                ).order_by('directory__sortkey')

                if children:
                    menu_item['children'] = []
                    add_contents(menu_item['children'], children)

                tree.append(menu_item)

        menu = []
        contents = ContentFile.objects.filter(
            directory__ref__siteref__site__domain=self.domain,
            directory__parent=None,
            language=self.language,
        ).order_by('directory__sortkey')
        add_contents(menu, contents)
        return menu

    @staticmethod
    def purge(domain, languages):
        for key in cache.iter_keys('dcl_sites:domain_languages:%s:*' % domain):
            if key.split(':')[-1] not in languages:
                cache.delete(key)


class DomainUrlCache(BaseCache):
    def __init__(self, domain, url):
        self.domain = domain.split(':')[0]
        self.url = url
        self.language = url.strip('/').split('/')[0]

    def get_key(self):
        return 'dcl_sites:domain_url:%s:%s' % (
            self.domain,
            self.url
        )

    def generate_data(self):
        content = ContentFile.objects.filter(
            directory__ref__siteref__site__domain=self.domain,
            url=self.url,
        ).select_related('directory').first()

        if not content:
            return {}

        domain = DomainCache(self.domain).get_data()

        return {
            'language': self.language,
            'languages_menu': self.get_languages_menu(content, domain),
            'content': content,
            'directory': content.directory,
            'children': self.get_children(content)
        }

    def get_children(self, content):
        return [
            {
                'title': c.title,
                'menu_title': c.menu_title,
                'url': c.url,
                'first_paragraph': c.first_paragraph,
            } for c in ContentFile.objects.filter(
                directory__in=content.directory.get_children()
            ).filter(
                language=self.language
            ).order_by('directory__sortkey')
        ]

    def get_languages_menu(self, content, domain):
        contents = content.directory.contentfile_set.all()
        languages = {
            c.language: {
                'url': c.url,
                'title': c.title,
                'menu_title': c.menu_title,
            } for c in contents
        }

        for code in domain['languages']:
            if code in languages:
                continue

            for c, language_name in settings.LANGUAGES:
                if c == code:
                    break

            languages[code] = {
                'url': '/%s/' % self.language,
                'title': language_name,
                'menu_title': language_name,
            }

        return languages

    @staticmethod
    def purge(domain, urls):
        for key in cache.iter_keys('dcl_sites:domain_url:%s:*' % domain):
            if key.split(':')[-1] not in urls:
                cache.delete(key)

from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand, CommandError

from dcl_sites.models import Repository, RepositoryRef
from dcl_sites.sync import sync_ref, sync_repository, sync_site
from dcl_sites.cache import DomainCache


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('repository', nargs='*')
        parser.add_argument('--domains', nargs='*')

    def handle(self, *args, **options):
        Site.objects.filter(domain='example.com').delete()

        for repository in options.get('repository', []):
            Repository.objects.get_or_create(url=repository)

        if not options.get('domains'):
            for repository in Repository.objects.all():
                sync_repository(repository)

        refs = RepositoryRef.objects.all()
        if options['domains']:
            refs = refs.filter(siteref__site__domain__in=options['domains'])

        for ref in refs:
            sync_ref(ref)

            for siteref in ref.siteref_set.all():
                sync_site(siteref.site)

        if not Site.objects.filter(domain='localhost'):
            Site.objects.create(domain='localhost', name='localhost')

        DomainCache.purge(Site.objects.values_list('domain', flat=True))

import json

from django import forms

from .models import ContentFile, Directory, RepositoryRef


class MetadataWidget(forms.Textarea):
    def render(self, name, value, attrs=None):
        attrs = attrs or {}
        attrs['data-widearea'] = 'enable'
        value = json.dumps(json.loads(value), indent=4)
        return super().render(name, value, attrs=attrs)



class ContentForm(forms.ModelForm):
    class Meta:
        model = ContentFile
        exclude = []
        widgets = dict(metadata=MetadataWidget)


class DirectoryForm(forms.ModelForm):
    class Meta:
        model = Directory
        exclude = []
        widgets = dict(metadata=MetadataWidget)


class RefForm(forms.ModelForm):
    class Meta:
        model = RepositoryRef
        exclude = []
        widgets = dict(metadata=MetadataWidget)

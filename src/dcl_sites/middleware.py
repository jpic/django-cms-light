from django import http

from dcl_sites.models import Alias
from dcl_sites.cache import DomainCache, DomainLangageCache, DomainUrlCache

from jinja2 import Environment, FileSystemLoader


class CurrentSiteMetadataMiddleware(object):
    """
    Middleware that sets `site` attribute to request object.
    """

    def process_request(self, request):
        request.dcl_sites = {
            'domain_cache': DomainCache(request.site.domain).get_data(),
        }

        if request.dcl_sites['domain_cache']['alias_of']:
            port = request.get_port()
            port_str = ':%s' % port if port != '80' else ''
            return http.HttpResponseRedirect(
                '%s://%s%s%s' % (
                    request.scheme,
                    request.dcl_sites['domain_cache']['alias_of'],
                    port_str,
                    request.path_info
                )
            )

        request.dcl_sites['domain_language_cache'] = DomainLangageCache(
            request.site.domain, request.LANGUAGE_CODE
        ).get_data()

        request.dcl_sites['domain_url_cache'] = DomainUrlCache(
            request.site.domain, request.path_info
        ).get_data()

        if not hasattr(self, 'environments'):
            self.environments = {}
        if request.site.domain not in self.environments:
            self.environments[request.site.domain] = Environment(
                loader=FileSystemLoader([
                    request.site.siteref.ref.website_path,
                ])
            )
        request.dcl_sites['domain_jinja2_environment'] = (
            self.environments[request.site.domain]
        )

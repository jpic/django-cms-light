from django.contrib import admin

from .models import (
    Alias,
    ContentFile,
    Repository,
    RepositoryRef,
    SiteRef,
    Directory,
)

admin.site.register(Alias)
admin.site.register(ContentFile)
admin.site.register(Repository)
admin.site.register(RepositoryRef)
admin.site.register(SiteRef)


class DirectoryAdmin(admin.ModelAdmin):
    list_display = (
        'rel_path',
        'sortkey',
    )
admin.site.register(Directory, DirectoryAdmin)

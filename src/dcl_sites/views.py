import markdown
import requests
import os

from django import http
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.views import generic

from jinja2 import Template

from .cache import DomainUrlCache

from .models import ContentFile, Directory, RepositoryRef
from .forms import ContentForm, DirectoryForm, RefForm
from .sync import sync_site


class PageView(generic.TemplateView):
    template_name = 'dcl_sites/page.html'

    def get(self, request, *args, **kwargs):
        if len(request.path_info.strip('/').split('/')) <= 1:
            return http.HttpResponseRedirect(
                request.dcl_sites['domain_language_cache']['index']
            )

        if 'content' not in request.dcl_sites['domain_url_cache']:
            raise http.Http404()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        md = markdown.Markdown(
            extensions=[
                'markdown.extensions.admonition',
                'markdown.extensions.codehilite',
                'markdown.extensions.extra',
                'markdown.extensions.headerid',
                'markdown.extensions.meta',
                'markdown.extensions.sane_lists',
                'markdown.extensions.smarty',
                'markdown.extensions.toc',
            ]
        )

        domain_language = self.request.dcl_sites['domain_language_cache']
        domain_url = self.request.dcl_sites['domain_url_cache']
        context = dict(
            request=self.request,
            children=domain_url.get('children', []),
            **domain_language['metadata']
        )

        env = self.request.dcl_sites['domain_jinja2_environment']
        template = env.get_template(
            '%s/%s.md' % (
                domain_url['directory'].rel_path,
                self.request.LANGUAGE_CODE
            )
        )
        preprocessed = template.render(**context)
        html = md.convert(preprocessed)
        return {
            'html': html,
        }


class StaffViewMixin(object):
    template_name = 'dcl_sites/staff.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return http.HttpResponseForbidden()

        if not request.user.is_staff:
            return http.HttpResponseForbidden()

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        result = super().form_valid(form)
        sync_site(self.request.site)
        return result

    def get_success_url(self):
        return self.request.path_info


class DomainStaffView(StaffViewMixin, generic.UpdateView):
    model = RepositoryRef
    form_class = RefForm

    def get_object(self):
        return self.request.site.siteref.ref


class DirectoryStaffView(StaffViewMixin, generic.UpdateView):
    model = Directory
    form_class = DirectoryForm

    def get_object(self):
        return Directory.objects.get(
            ref__siteref__site__domain=self.request.get_host().split(':')[0],
            contentfile__url=self.kwargs['url'],
        )


class ContentStaffView(StaffViewMixin, generic.UpdateView):
    model = ContentFile
    form_class = ContentForm

    def get_object(self):
        return ContentFile.objects.get(
            directory__ref__siteref__site__domain=self.request.get_host().split(':')[0],
            url=self.kwargs['url'],
        )

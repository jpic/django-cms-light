import os
import re

from annoying.fields import AutoOneToOneField

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.db import models

from mptt.models import MPTTModel, TreeForeignKey


class Alias(models.Model):
    alias = models.ForeignKey(Site, related_name='alias_of')
    source = models.ForeignKey(Site, related_name='aliases')

    def __str__(self):
        return '%s -> %s' % (self.alias, self.source)


class Repository(models.Model):
    url = models.CharField(max_length=255)

    def __str__(self):
        return self.url


class RepositoryRef(models.Model):
    repository = models.ForeignKey(Repository)
    name = models.CharField(max_length=100)
    metadata = JSONField(default={}, blank=True)
    style = models.TextField(default='', blank=True)
    script = models.TextField(default='', blank=True)
    extra_head = models.TextField(default='', blank=True)
    extra_body = models.TextField(default='', blank=True)
    index = models.ForeignKey(
        'Directory',
        related_name='index_of',
        null=True
    )

    def __str__(self):
        return self.name

    @property
    def repo_path(self):
        return os.path.join(
            settings.MEDIA_ROOT,
            'refs',
            str(self.pk),
        )

    @property
    def website_path(self):
        return os.path.join(
            self.repo_path,
            self.metadata.get('root', 'website')
        )

    @property
    def repo_url(self):
        return ''.join((
            settings.MEDIA_URL,
            'refs/',
            str(self.pk),
        ))

    @property
    def website_url(self):
        return ''.join((
            settings.MEDIA_URL,
            'refs/',
            str(self.pk),
            '/',
            self.metadata.get('root', 'website'),
        ))


class SiteRef(models.Model):
    site = AutoOneToOneField(Site, primary_key=True)
    ref = models.ForeignKey(RepositoryRef)


class Directory(MPTTModel):
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True
    )
    ref = models.ForeignKey(RepositoryRef)
    rel_path = models.CharField(max_length=255)
    background = models.ImageField(null=True)
    metadata = JSONField(default={}, blank=True)
    sortkey = models.PositiveIntegerField(default=0)
    publish_datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.rel_path

    class Meta:
        ordering = ('sortkey', 'publish_datetime')
        unique_together = (
            (
                'ref',
                'rel_path',
            ),
        )


class ContentFile(models.Model):
    directory = models.ForeignKey(Directory)
    language = models.CharField(max_length=5, db_index=True)
    source = models.TextField()
    metadata = JSONField(default={}, blank=True)
    url = models.CharField(max_length=255)

    def __str__(self):
        return self.url

    @property
    def title(self):
        return ' '.join(self.metadata['title'])

    @property
    def menu_title(self):
        return ' '.join(
            self.metadata.get(
                'menu-title',
                [self.title]
            )
        )

    @property
    def first_paragraph(self):
        paragraph = []
        has_more = False

        for line in self.source.split('\n'):
            if re.match('^[\w\d]', line) is None:
                if not paragraph:
                    continue
                else:
                    break

            paragraph.append(line)
        self._first_paragraph = '\n'.join(paragraph)

    class Meta:
        unique_together = (
            (
                'directory',
                'language',
            ),
        )

from PIL import (
    Image,
    ImageEnhance,
    ImageFilter,
    ImageOps,
)


def get_translated_dict(language, data):
    result = {}

    for key, value in data.items():
        if isinstance(value, dict):
            if language in value:
                result[key] = value[language]
            else:
                result[key] = get_translated_dict(language, value)
        elif isinstance(value, list) and len(value):
            result[key] = [
                get_translated_dict(language, v)
                if isinstance(v, dict) else v
                for v in value
            ]
        else:
            result[key] = value
    return result


def process_image(i, bgconf):
    FILTERS = (
        'BLUR',
        'CONTOUR',
        'DETAIL',
        'EDGE_ENHANCE',
        'EDGE_ENHANCE_MORE',
        'EMBOSS',
        'FIND_EDGES',
        'SMOOTH',
        'SMOOTH_MORE',
        'SHARPEN',
    )
    TRANSPOSITIONS = (
        'FLIP_LEFT_RIGHT',
        'FLIP_TOP_BOTTOM',
        'ROTATE_90',
        'ROTATE_180',
        'ROTATE_270',
    )
    CONTRAST_METHODS = (
        'enhance',
    )
    OPS = (
        'autocontrast',
        'putpalette',
    )
    def linear_ramp(r, g, b):
        ramp = []
        for i in range(255):
            ramp.extend(
                (
                    int(r*i/255),
                    int(g*i/255),
                    int(b*i/255),
                )
            )
        return ramp
    RAMP_FUNCTIONS = {
        'linear': linear_ramp,
    }

    ramps = {}
    for name, data in bgconf.get('ramps', {}).items():
        method = RAMP_FUNCTIONS[data.pop('function')]
        ramps[name] = method(**data)

    for operation in bgconf.get('operations', []):
        method = None
        for key, value in operation.items():
            method = key
            arg = value
            break

        if method == 'transpose' and arg in TRANSPOSITIONS:
            i = i.transpose(getattr(Image, arg))
        elif method == 'convert' and arg in ('L', 'RGB'):
            i = i.convert(arg)
        elif method == 'autocontrast':
            i = ImageOps.autocontrast(i)
        elif method == 'putpalette':
            i.putpalette(ramps[arg])
        elif method == 'filter' and arg in FILTERS:
            i.filter(getattr(ImageFilter, arg))
        elif method == 'point_multiply':
            i = i.point(lambda p: p * arg)
        elif method == 'contrast':
            e = ImageEnhance.Contrast(i)
            for contrast in arg:
                for contrast_method, contrast_arg in contrast.items():
                    break
                if contrast_method in CONTRAST_METHODS:
                    getattr(e, contrast_method)(contrast_arg)
    return i

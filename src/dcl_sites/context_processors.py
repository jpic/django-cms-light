def context_cache_short_variables(request):
    return {
        'domain': request.dcl_sites['domain_cache'],
        'language': request.dcl_sites['domain_language_cache'],
        'url': request.dcl_sites['domain_url_cache'],
    }

# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-20 11:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dcl_sites', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='directory',
            name='extra_body',
        ),
        migrations.RemoveField(
            model_name='directory',
            name='extra_head',
        ),
        migrations.RemoveField(
            model_name='directory',
            name='script',
        ),
        migrations.RemoveField(
            model_name='directory',
            name='style',
        ),
    ]
